# Project Manager #

Sistema para gerenciamento de projetos:


A sql do banco encontra-se dentro do diretório: documentacao

**Usuário:** admin@admin.com.br
**Senha:** admin

Para ver uma demonstração do projeto, basta acessar a url: [http://project-manager.890m.com/](http://project-manager.890m.com/)


**Observação:** O projeto ainda está em desenvolvimento, portanto há vários "cruds" prontos e que não estão linkados no menu da administração.

![projectmanager.png](https://bitbucket.org/repo/XREA6k/images/910586038-projectmanager.png)

### Recursos existentes ###

* Gerenciamento de Clientes
* Gerenciamento de Projetos
* Gerenciamento de Tarefas
* Gerenciamento de Bugs
* Gerenciamento de Usuários
* Gerenciamento de Chamados (incompleto)
* Resumo de tudo que há no sistema na dashboard
* Lista de projetos existentes na dashboard
* Gráfico com andamento da execuções de todas as tarefas na dashboard
* Gráfico com andamento das correções de todos os bugs na dashboard
* Gráfico com todos os projetos mostrando números de tarefas e bugs para cada um